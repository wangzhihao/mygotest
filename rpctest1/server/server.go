package main

import (
	"log"
	"mytest/rpctest1"
	"net"
	"net/rpc"
)

func main() {

	rpc.RegisterName("HelloService", new(rpctest1.HelloService))

	listener, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal("ListenTCP error:", err)
	}

	conn, err := listener.Accept()
	if err != nil {
		log.Fatal("Accept error:", err)
	}

	rpc.ServeConn(conn)
}
