package main


import (
"fmt"
"net/http"
	"strings"
)

func myWeb(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "这是一个开始")
	paths:=strings.Split(r.URL.Path,"/")
	fmt.Printf("paths[0]:%v \n",paths[0])
	fmt.Printf("paths[1]:%v \n",paths[1])
	fmt.Printf("paths[2]:%v \n",paths[2])
}

func main() {
	http.HandleFunc("/a/b", myWeb)
	fmt.Println("服务器即将开启，访问地址 http://localhost:8080")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println("服务器开启错误: ", err)
	}
}

