package mytest

import (
	"fmt"
	"testing"
)

func TestClassifier(t *testing.T){
	classifier(1,-11.7,"ZIP",nil,true)
}

func classifier(items... interface{}){
	for i,x:=range items{
		switch x.(type) {
		case bool:
			fmt.Printf("param #%d is a bool\n",i)
		case float64:
			fmt.Printf("param #%d is a float64\n",i)
		case int,int8,int16,int32,int64:
			fmt.Printf("param #%d is a int\n",i)
		case nil:
			fmt.Printf("param #%d is a nil\n",i)
		case string:
			fmt.Printf("param #%d is a string\n",i)
		default:
			fmt.Printf("param #%d's type is unknown\n",i)
		}
	}
}