package main

import (
	"io"
	rpctest2 "mytest/rpctest2"
	"net/http"
	"net/rpc"
	"net/rpc/jsonrpc"
)

//post request:curl localhost:1234/jsonrpc -X POST \ --data '{"method":"HelloService.Hello","params":["hello"],"id":0}'
func main() {
	rpc.RegisterName("HelloService", new(rpctest2.HelloService))

	http.HandleFunc("/jsonrpc", func(w http.ResponseWriter, r *http.Request) {
		var conn io.ReadWriteCloser = struct {
			io.Writer
			io.ReadCloser
		}{
			ReadCloser: r.Body,
			Writer:     w,
		}

		rpc.ServeRequest(jsonrpc.NewServerCodec(conn))
	})

	http.ListenAndServe(":1234", nil)
}