package jsontest

import (
	"encoding/json"
	"fmt"
	"testing"
)

type Info struct {
	Name string
	Age  int
	Sex  string
}

func TestPeople(t *testing.T) {
	infos := []Info{
		{
			Name: "Sophia",
			Age:  23,
			Sex:  "female",
		},
		{
			Name: "Benjie",
			Age:  24,
			Sex:  "male",
		},
	}
	data, err := json.Marshal(infos)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(data)
	fmt.Println(string(data))

	/*
		    [91 123 34 78 97 109 101 34 58 34 83 111 112 104 105 97 34 44 34 65 103 101 34 58 50 51 44 34 83 101 120 34 58 34 102 101 109 9
		7 108 101 34 125 44 123 34 78 97 109 101 34 58 34 66 101 110 106 105 101 34 44 34 65 103 101 34 58 50 52 44 34 83 101 120 34 58
		 34 109 97 108 101 34 125 93]
		    [{"Name":"Sophia","Age":23,"Sex":"female"},{"Name":"Benjie","Age":24,"Sex":"male"}]
	*/
}



