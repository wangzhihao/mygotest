package mytest

import (
	"fmt"
	"strings"
	"testing"
)

func TestFactory(t *testing.T)  {
	addZip:=makeAddSuffix(".zip")
	addTgz:=makeAddSuffix(".tar.gz")
	fmt.Println(addTgz("filename"),addZip("filename"),addZip("gobook.zip"))
}

func makeAddSuffix(suffix string) func(string) string{
	return func(name string) string{
		if !strings.HasSuffix(name,suffix){
			return  name+suffix
		}
		return name
	}
}
