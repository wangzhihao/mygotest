package mytest

import (
	"fmt"
	"testing"
	"time"
)
var now = time.Now()
func TestInit(t *testing.T) {
	fmt.Println("main:", int(time.Now().Sub(now).Seconds()))
}
func init() {
	fmt.Println("init:", int(time.Now().Sub(now).Seconds()))
	w := make(chan bool)
	go func() {
		time.Sleep(time.Second * 3)
		w <- true
	}()
	<-w
}