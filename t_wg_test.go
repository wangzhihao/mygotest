package mytest

import (
	"sync"
	"testing"
)

func TestWg(t *testing.T) {
	wg:=sync.WaitGroup{}

	wg.Add(2)

	go func() {
		println("go 1")
		wg.Done()
	}()

	go func() {
		println("go 2")
		wg.Done()
	}()

	wg.Wait()
}