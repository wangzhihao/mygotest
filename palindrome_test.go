package mytest

import (
	"fmt"
	"testing"
	"unicode/utf8"
)

func TestPalindrome(t *testing.T) {
	fmt.Println(	IsPalindrome("cotrtyoc"))
}

func IsPalindrome(word string) bool {
	if utf8.RuneCountInString(word)<=1{
		return true
	}
	first,sizeOfFirst:=utf8.DecodeRuneInString(word)
	last,sizeOfLast:=utf8.DecodeLastRuneInString(word)
	if first!=last{
		return false
	}
	return IsPalindrome(word[sizeOfFirst:len(word)-sizeOfLast])

}
