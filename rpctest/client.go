package main

import (
	"fmt"
	"log"
	"net/rpc"
)
func main() {
	client, err := rpc.Dial("tcp", "192.168.140.129:1234")
	if err != nil {
		log.Fatal("dialing:", err)
	}

	var reply string
	err = client.Call("HelloService.Hello", "sun", &reply)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(reply)
}