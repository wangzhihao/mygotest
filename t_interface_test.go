package mytest

import (
	"fmt"
	"testing"
)

type Stringer interface {
	String() string
}

type Printer interface {
	Stringer
	Print()
}

type User struct {
	id  int
	name string
}

func (self *User) String() string  {
	return fmt.Sprintf("User is %d,%s",self.id,self.name)
}

func (self *User) Print()  {
	println(self.String())
}

func TestInterface(tt *testing.T) {
	var t Printer=&User{1,"Tom"}
	t.Print()
}