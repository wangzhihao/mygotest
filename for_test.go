package mytest

import (
	"fmt"
	"testing"
)

func TestFor(t *testing.T) {
	var Hosts  = [][]string{{"a:3030","a:3040"},{"b:3030","b:3040"},{"c:3030","c:3040"}}
	hosts := make([]string, len(Hosts))
	for i := range Hosts {
		hosts[i] = Hosts[i][1]
	}
	fmt.Print(hosts)
}
