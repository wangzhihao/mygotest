package mytest

import "testing"

func TestChannel(t *testing.T) {
	data := make(chan int)
	exit:=make(chan bool)
	go func() {
		for d:=range data{
			println(d)
		}
		println("recv over.")
		exit<-true
	}()

	data<-1
	data<-2
	data<-3
	close(data)

	println("send over.")

	<-exit
}