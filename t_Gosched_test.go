package mytest

import (
	"runtime"
	"sync"
	"testing"
)

func TestGoschedaa(t *testing.T) {
	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		defer wg.Done()
		for i := 0; i < 6; i++ {
			println(i)
			if i == 3 {
				runtime.Gosched()
			}
		}
	}()
	go func() {
		defer wg.Done()
		for i:=0;i<100;i++{
			println("Hello, World!")
			if i==2{
				runtime.Gosched()
			}
		}
	}()
	wg.Wait()
}
