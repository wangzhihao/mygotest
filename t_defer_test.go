package mytest

import (
	"runtime"
	"sync"
	"testing"
)

func TestDefer(t *testing.T) {
	wg := new(sync.WaitGroup)
	wg.Add(1)
	go func() {
		defer wg.Done()
		defer println("A.defer")
		func() {
			defer println("B.defer")
			runtime.Goexit() // 终⽌止当前 goroutine
			println("B") // 不会执⾏行
		}()
		println("A") // 不会执⾏行
	}()
	wg.Wait()
}
