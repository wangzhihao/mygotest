package mytest

import (
	"fmt"
	"os"
	"testing"
)

func TestDir(t *testing.T) {
	dir,err:=os.Open("/home/oliver/test")
	if err!=nil{
		panic(err)
	}

	fis,err:=dir.Readdir(-1)
	for _, fi := range fis {
		if fi.IsDir(){
			fmt.Printf("invalid file, fi.IsDir() is true,%v \n",fi.Name())
		}else {
			fmt.Printf("filename is:%v \n",fi.Name())
		}
	}
}
