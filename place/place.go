package place

type People struct {
	id int
	age int
}

func New(id,age int) *People{
	return &People{id,age}
}

func(people *People) Age() int {
	return people.age
}


