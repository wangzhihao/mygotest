package mytest

import (
	"fmt"
	"reflect"
	"testing"
)
type user struct {
	Username string
}
type Admin struct{
	user
	title string
}

func TestReflect(tt *testing.T) {
	u := new(Admin)
	t := reflect.TypeOf(u)
	if t.Kind() == reflect.Ptr {
			t = t.Elem()
	}
	for i, n := 0, t.NumField(); i < n; i++ {
		f := t.Field(i)
		fmt.Println(f.Name, f.Type)
	}
}
